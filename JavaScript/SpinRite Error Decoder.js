// https://jscalc.io/calc/ulc8qxJNU5nIqFbt

'use strict';

var errorDefinitions = [
    'Bad block',
    'Uncorrected error',
    'GRC timeout',
    'ID not found',
    'GRC udma / sata link error',
    'Command aborted',
    'Track 0 not found',
    'Address mark not found',
    'Drive busy',
    'Drive ready',
    'Device fault',
    'Service (overlap/queued commands)',
    'Data request (DRQ)',
    'Corrected (obsolete)',
    'Index (obsolete)',
    'Error (bits in high byte are set)',
];

// From: https://www.ctyme.com/intr/rb-0606.htm#Table234
var biosDefinitions = {
    "00": "successful completion",
    "01": "invalid function in AH or invalid parameter",
    "02": "address mark not found",
    "03": "disk write-protected",
    "04": "sector not found/read error",
    "05": "reset failed (hard disk)",
    "06": "disk changed (floppy)",
    "07": "drive parameter activity failed (hard disk)",
    "08": "DMA overrun",
    "09": "data boundary error (attempted DMA across 64K boundary or >80h sectors)",
    "0A": "bad sector detected (hard disk)",
    "0B": "bad track detected (hard disk)",
    "0C": "unsupported track or invalid media",
    "0D": "invalid number of sectors on format (PS/2 hard disk)",
    "0E": "control data address mark detected (hard disk)",
    "0F": "DMA arbitration level out of range (hard disk)",
    "10": "uncorrectable CRC or ECC error on read",
    "11": "data ECC corrected (hard disk)",
    "20": "controller failure",
    "31": "no media in drive (IBM/MS INT 13 extensions)",
    "32": "incorrect drive type stored in CMOS (Compaq)",
    "40": "seek failed",
    "80": "timeout (not ready)",
    "AA": "drive not ready (hard disk)",
    "B0": "volume not locked in drive (INT 13 extensions)",
    "B1": "volume locked in drive (INT 13 extensions)",
    "B2": "volume not removable (INT 13 extensions)",
    "B3": "volume in use (INT 13 extensions)",
    "B4": "lock count exceeded (INT 13 extensions)",
    "B5": "valid eject request failed (INT 13 extensions)",
    "B6": "volume present but read protected (INT 13 extensions)",
    "BB": "undefined error (hard disk)",
    "CC": "write fault (hard disk)",
    "E0": "status register error (hard disk)",
    "FF": "sense operation failed (hard disk)"
};



function convertErrorCode(errorCode) {

    // Convert the hex characters to a string of 1s and 0s.
    var binaryCode = parseInt(errorCode, 16).toString(2);

    // Pad the string so that it's always 16 characters long.
    while (binaryCode.length < 16) {

        var binaryCode = '0' + binaryCode;

    }

    var outputDefinitions = [];

    // Loop through the binary string.
    for (var index = 0; index < 16; index++) {

        // Check if the current bit is 1.
        if (binaryCode[index] == '1') {

            // Look up the bit's definition, then build an object.
            var defObj = { Definitions: errorDefinitions[index] };

            // Add the object to the output array.
            outputDefinitions.push(defObj);

        }

    }

    return outputDefinitions;

}



if (inputs.ErrorCode) {

    // Codes in versions before Alpha 20 were 4 characters long.
    if ((inputs.ErrorCode.length == 4) && (inputs.ShortCode)) {

        // Output the table of converted values.
        return {

            ErrorStatusTable: convertErrorCode(inputs.ErrorCode)

        };

    // Look for 3-part codes.
    } else if (inputs.ErrorCode.length > 10) {

        var upperCode = inputs.ErrorCode.toUpperCase();
        var splitCode = upperCode.split(":");
        var leftCode = splitCode[0];
        var middleCode = splitCode[1];
        var rightCode = splitCode[2];

        // SPIN doesn't tell us which code set to use.
        if (rightCode.startsWith('S')) {

            throw "SPIN cannot be used in this calculator. Please use AHCI, ATA, BIOS, or IDE.";

        }

        // BIOS.
        if (rightCode.startsWith('B')) {

            var leftBiosCode = leftCode.substr(0,2);
            var middleBiosCode = middleCode.substr(0,2);

            // Output the tables of converted values.
            return {

                ErrorStatusTable: [{Definitions: biosDefinitions[leftBiosCode]}],
                NonErrorStatusTable: [{Definitions: biosDefinitions[middleBiosCode]}]

            };

        // Everything else, such as: AHCI, DOS, SPIN.
        } else {

            // Output the tables of converted values.
            return {

                ErrorStatusTable: convertErrorCode(leftCode),
                NonErrorStatusTable: convertErrorCode(middleCode)

            };

        }

    } else {

        // Prevent an error when input is incomplete.
        return {

            ErrorStatusTable: [{ Definitions: '' }]

        };

    }

} else {

    // Prevent an error when input is empty.
    return {

        ErrorStatusTable: [{ Definitions: '' }]

    };

}