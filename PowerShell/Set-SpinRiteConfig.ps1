<#
.SYNOPSIS
Creates or updates SpinRite config files (.CFG).

This script is only accurate with Alpha 9 and later.
Don't use 'After' or 'Before & After' for -Benchmark in a config destined for a version before Alpha 19!

.NOTES
Version 002

You can use tab-completion to cycle through the available values of -Benchmark and -Sentences.

If you've never run PowerShell before, here's a quick guide:
https://blog.udemy.com/run-powershell-script/

This script lives here:
https://gitlab.com/ColbyBouma/misc-scripts/-/blob/master/PowerShell/Set-SpinRiteConfig.ps1

Use this link to download it:
https://gitlab.com/ColbyBouma/misc-scripts/-/raw/master/PowerShell/Set-SpinRiteConfig.ps1?inline=false

.EXAMPLE
.\Set-SpinRiteConfig.ps1 'E:\srpr22.cfg' 3 $true $true 'Phrases' 10 'Before'

Set Scan Level to 3, enable logging, enable deleting oldest logs, use phrases, set the screen saver timeout to 10,
and set benchmarking to Before.

.EXAMPLE
.\Set-SpinRiteConfig.ps1 -Path 'E:\srpr22.cfg' -ScanLevel 4

Set Scan Level to 4.
If srpr22.cfg exists, only Scan Level will be changed.
If it does not exist, it will be created, and the defaults will be used for the remaining settings.
#>

[CmdletBinding()]
param (
    # The path to the SpinRite config file you want to create or update.
    [Parameter(Mandatory = $true)]
    [String]$Path,

    [ValidateRange(1, 4)]
    [Int16]$ScanLevel = 2,

    [Bool]$EnableLog = $true,

    [Bool]$DeleteOldest = $false,

    [ValidateSet('Phrases', 'Sentences')]
    [String]$Sentences = 'Sentences',

    [ValidateRange(0, 999)]
    [Int16]$ScreenSaverTimeout = 0,

    [ValidateSet('Never', 'Before', 'After', 'Before & After')]
    [String]$Benchmark = 'Never'
)

$Exists = $true

if ( -not (Test-Path -Path $Path) ) {

    $Exists = $false

}



$CfgName = Split-Path -Path $Path -Leaf

if ( $Exists ) {
    
    $CfgSize = (Get-ItemProperty -Path $Path).Length

    if ( $CfgSize -ne 12 ) {

        throw "'$CfgName' is not a valid SpinRite config file."

    }

    $CfgBytes = (Format-Hex -Path $Path).Bytes

} else {

    $CfgBytes = New-Object Byte[] -ArgumentList 12

}

Write-Verbose "Before: $((Format-Hex -InputObject $CfgBytes).HexBytes)"



# Convert the provided values into byte pairs.
if ( ('ScanLevel' -in $PSBoundParameters.Keys) -or (-not $Exists) ) {

    # I have to do it this way because the validation is sticky.
    [Int16]$ScanLevelAdjusted = $ScanLevel - 1
    $CfgBytes[0], $CfgBytes[1] = [BitConverter]::GetBytes($ScanLevelAdjusted)

}

if ( ('EnableLog' -in $PSBoundParameters.Keys) -or (-not $Exists) ) {

    $CfgBytes[2], $CfgBytes[3] = [BitConverter]::GetBytes($EnableLog)

}

if ( ('DeleteOldest' -in $PSBoundParameters.Keys) -or (-not $Exists) ) {

    $CfgBytes[4], $CfgBytes[5] = [BitConverter]::GetBytes($DeleteOldest)

}

if ( ('Sentences' -in $PSBoundParameters.Keys) -or (-not $Exists) ) {

    [Int16]$SentenceBytes = switch ($Sentences) {
        'Phrases' { 0 }
        'Sentences' { 1 }
    }

    $CfgBytes[6], $CfgBytes[7] = [BitConverter]::GetBytes($SentenceBytes)

}

if ( ('ScreenSaverTimeout' -in $PSBoundParameters.Keys) -or (-not $Exists) ) {

    $CfgBytes[8], $CfgBytes[9] = [BitConverter]::GetBytes($ScreenSaverTimeout)

}

if ( ('Benchmark' -in $PSBoundParameters.Keys) -or (-not $Exists) ) {

    [Int16]$BenchmarkBytes = switch ($Benchmark) {
        'Never' { 0 }
        'Before' { 1 }
        'After' { 2 }
        'Before & After' { 3 }
    }
        
    $CfgBytes[10], $CfgBytes[11] = [BitConverter]::GetBytes($BenchmarkBytes)

}

Write-Verbose "After : $((Format-Hex -InputObject $CfgBytes).HexBytes)"



# Static methods like [IO.File]::WriteAllBytes() require an absolute path.
if ( -not $Exists ) {

    $Parent = Split-Path -Path $Path -Parent
    if ( -not $Parent ) {

        $Path = Join-Path -Path $PWD -ChildPath $Path

    } else {

        $ResolvedParent = Resolve-Path -Path $Parent
        $Path = Join-Path -Path $ResolvedParent -ChildPath $CfgName

    }

    Write-Verbose $Path
    
} else {

    $Path = Resolve-Path -Path $Path

}

[IO.File]::WriteAllBytes($Path, $CfgBytes)