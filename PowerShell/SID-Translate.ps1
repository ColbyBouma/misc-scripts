# Created by Colby Bouma
# This script translates a SID into a username or vice versa
# I created the original version of this script at my previous job. Unfortunately I did not cite my sources.
#
# v 001

[CmdletBinding()]
param (
    [Parameter(
        Mandatory = $true,
        Position = 0,
        ValueFromPipeline = $true,
        HelpMessage = "Please specify a username or SID to translate.")]
    [ValidateNotNullOrEmpty()]
    [string]
    $InputString
)

if ( $InputString.StartsWith("S-1-5-21") ) {
# Input string is a SID

    $InputObject  = New-Object System.Security.Principal.SecurityIdentifier ($InputString)
    $OutputObject = $InputObject.Translate([System.Security.Principal.NTAccount])

} else {
# Input string is a username

    $InputObject  = New-Object System.Security.Principal.NTAccount ($InputString)
    $OutputObject = $InputObject.Translate([System.Security.Principal.SecurityIdentifier])

}

$OutputObject.Value