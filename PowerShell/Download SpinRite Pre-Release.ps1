<#
.SYNOPSIS
This script downloads the currently available SpinRite 6.1 Release Candidate.

.NOTES
Version 020

You MUST provide your transaction code or serial number!

If you've never run PowerShell before, here's a quick guide:
https://blog.udemy.com/run-powershell-script/

The downloaded file will automatically be named according to the number of srpr-rc*.exe files the destination folder contains,
and the current value of:
https://gitlab.com/ColbyBouma/misc-scripts/-/raw/master/Text/SpinRite-RC-Version.txt

This script lives here:
https://gitlab.com/ColbyBouma/misc-scripts/-/blob/master/PowerShell/Download%20SpinRite%20Pre-Release.ps1

Use this link to download it:
https://gitlab.com/ColbyBouma/misc-scripts/-/raw/master/PowerShell/Download%20SpinRite%20Pre-Release.ps1?inline=false

.EXAMPLE
& '.\Download SpinRite Pre-Release.ps1' -TransactionCode XXXXXXXXXXXXX -DestinationFolder 'C:\Your\Desired\Folder'

Downloads the current Alpha to the folder you specified.

.EXAMPLE
& '.\Download SpinRite Pre-Release.ps1' -TransactionCode XXXXXXXXXXXXX -Suffix 'a10'

Downloads Alpha 10 to the current folder.
#>

[CmdletBinding()]
param (
    [Parameter(Mandatory = $true)]
    # The transaction code or serial number from your SpinRite purchase email.
    [String]$TransactionCode,

    # The folder you would like to save SpinRite to.
    [String]$DestinationFolder = $PWD,

    # Used for downloading special builds or previous releases, such as 'dlt' or 'a8'.
    [String]$Suffix,

    # Compatibility for sr61pr.
    [String]$Name
)

# Stop execution if an error occurs.
$ErrorActionPreference = 'Stop'

# Get the current Alpha version. This is manually updated and could be out of date, so I treat it as the minimum version.
$MinimumVersion = [Int32][System.Net.WebClient]::new().DownloadString(
    'https://gitlab.com/ColbyBouma/misc-scripts/-/raw/master/Text/SpinRite-RC-Version.txt'
)

if ( -not (Test-Path $DestinationFolder) ) {

    throw "'$DestinationFolder' does not exist, or cannot be accessed."

}

# Make sure it's an absolute path.
$DestinationFolder = (Resolve-Path $DestinationFolder).Path

if ( $Name ) {

    if ( $Name.EndsWith('.exe') ) {

        $PRName = $Name

    } else {

        $PRName = '{0}.exe' -f $Name

    }

} else {

    if ( $Suffix ) {

        # Remove any hyphens, then build the name the downloaded file will be saved as.
        $Suffix = $Suffix.Trim('-')
        $PRName = 'srpr-{0}.exe' -f $Suffix

    } else {

        # Find all normal RCs: 'srpr-rc' + any numbers + '.exe'.
        # It's sorted by LastWriteTime because the names are strings, so they would sort as 1, 10, 2, etc.
        $ExistingPRs = Get-ChildItem -Path $DestinationFolder -Filter 'srpr-rc*.exe' |
        Where-Object Name -match 'srpr-rc\d+\.exe' |
        Sort-Object LastWriteTime
        $ExistingPRCount = $ExistingPRs.Count

        # If you haven't downloaded every RC, use $MinimumVersion to name the downloaded file.
        if ( ($ExistingPRCount + 1) -lt $MinimumVersion ) {

            $ExistingPRCount = $MinimumVersion - 1

        }

        # Grab the last entry in the list, because it's the newest.
        $PreviousPRName = ($ExistingPRs | Select-Object -Last 1).Name
        $PreviousPRPath = Join-Path -Path $DestinationFolder -ChildPath $PreviousPRName
        $PRName = 'srpr-rc{0}.exe' -f ($ExistingPRCount + 1)
    
    }

}

$PRPath = Join-Path -Path $DestinationFolder -ChildPath $PRName

if ( Test-Path $PRPath ) {

    throw "$PRName already exists."

}

# I figured out what to use for Uri and Body by using the Developer Tools while poking around
# https://www.grc.com/prerelease.htm
$Params = @{
    'Method'      = 'Post'
    'Uri'         = 'https://www.grc.com/x/ne.dll?qg1go2tu'
    'Body'        = '1={0}&2={1}' -f $TransactionCode, 'Get+Download+Link'
    'ErrorAction' = 'Stop'
}

# Download the page that contains your one-time-use download link, and extract it with regex.
$DownloadLink = (Invoke-RestMethod @Params | Select-String 'https://www.grc.com/\w+/srpr.exe').Matches.Value
if ( $Suffix -or $Name ) {

    # Add the suffix to the download link.
    $DownloadLink = $DownloadLink -replace 'srpr\.exe', $PRName

}
Write-Verbose $DownloadLink

# Download the file. WebClient is WAY faster than Invoke-WebRequest.
[System.Net.WebClient]::new().DownloadFile($DownloadLink, $PRPath)

if ( $Suffix -or (-not $PreviousPRName) ) {

    # I still want the path to the downloaded file to be written, so pretend files were hashed and they didn't match.
    $HashTest = $false

} else {

    # See if the downloaded file is actually new.
    $HashTest = (Get-FileHash $PreviousPRPath).Hash -eq (Get-FileHash $PRPath).Hash

}

if ( $HashTest ) {

    Write-Warning "$PRName is identical to $PreviousPRName and will be deleted."
    Remove-Item $PRPath

} else {

    # Output information about the downloaded file.
    $PRLength = (Get-ChildItem $PRPath).Length
    [PSCustomObject]@{
        'Download Path' = $PRPath
        'Size Bytes'    = '{0:n0}' -f $PRLength
        'Size KiB'      = '{0} KiB' -f [Math]::Round(($PRLength / 1KB), 1)
    }

}