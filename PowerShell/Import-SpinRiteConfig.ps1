<#
.SYNOPSIS
Reads SpinRite config files (.CFG).

This script is only accurate with Alpha 9 and later.
There were only 2 benchmark options before Alpha 19, and they had different names, but they mean the same thing.

.NOTES
Version 002

If you've never run PowerShell before, here's a quick guide:
https://blog.udemy.com/run-powershell-script/

This script lives here:
https://gitlab.com/ColbyBouma/misc-scripts/-/blob/master/PowerShell/Import-SpinRiteConfig.ps1

Use this link to download it:
https://gitlab.com/ColbyBouma/misc-scripts/-/raw/master/PowerShell/Import-SpinRiteConfig.ps1?inline=false

.EXAMPLE
.\Import-SpinRiteConfig.ps1 -Path 'E:\srpr22.cfg'
#>

[CmdletBinding()]
param (
    # The path to the SpinRite config file you want to import.
    [Parameter(Mandatory = $true)]
    [String]$Path
)

if ( -not (Test-Path -Path $Path) ) {

    throw 'Unable to find the specified file.'

}

$CfgName = Split-Path -Path $Path -Leaf
$CfgSize = (Get-ItemProperty -Path $Path).Length

if ( $CfgSize -ne 12 ) {

    throw "'$CfgName' is not a valid SpinRite config file."

}

# Read the config file.
$CfgHex = Format-Hex -Path $Path
$CfgBytes = $CfgHex.Bytes

$CfgDecimal = @()
for ( $Index = 0; $Index -le 10; $Index += 2 ) {
    
    # Grab a Little-endian byte pair.
    # Accessing [Byte[]] by index returns [Object[]], but [BitConverter] requires [Byte[]], so I have to cast it back.
    $BytePair = [Byte[]]$CfgBytes[$Index, ($Index + 1)]

    $CfgDecimal += [BitConverter]::ToInt16($BytePair)

}

[PSCustomObject]@{
    'Scan Level'           = $CfgDecimal[0] + 1
    'Log enabled'          = [Bool]$CfgDecimal[1]
    'Delete oldest'        = [Bool]$CfgDecimal[2]
    'Sentences or phrases' = switch ($CfgDecimal[3]) {
        0 { 'Phrases' }
        1 { 'Sentences' }
    }
    'Screen saver timeout' = $CfgDecimal[4]
    'Benchmark'            = switch ($CfgDecimal[5]) {
        0 { 'Never' }
        1 { 'Before' }
        2 { 'After' }
        3 { 'Before & After' }
    }
    'Bytes'                = $CfgHex.HexBytes
}