# Requested by Steve Gibson: https://grc.com/groups/securitynow:39703
# Based on: https://serverfault.com/a/1067232

# Download the JSON file and convert it into a PowerShell object.
$TsssUri = 'https://raw.githubusercontent.com/chromium/chromium/main/net/http/transport_security_state_static.json'
$TsssJson = [System.Net.WebClient]::new().DownloadString($TsssUri) | ConvertFrom-Json

# Search for entries where include_subdomains is true, and the name does not contain a period.
($TsssJson.entries | Where-Object {($_.'include_subdomains' -eq $true) -and ($_.name -notmatch '\.')}).name | Sort-Object