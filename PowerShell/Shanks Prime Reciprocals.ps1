
# https://www.youtube.com/watch?v=DmfxIhmGPP4

[CmdletBinding()]
param (
    [Parameter(Mandatory = $true)]
    [Double]$InputPrime
)

$Remainders = @{
    1 = $true
}

# Start the remainder at the power of 10 that is larger than the input.
$Remainder = [Math]::Pow(10, ("$InputPrime").Length)

for ( $Attempt = 0; $Attempt -le $InputPrime; $Attempt ++ ) {

    $Remainder = $Remainder % $InputPrime
    Write-Verbose $Remainder

    if ( $Remainder -eq 0 ) {

        throw 'Remainder should never be zero. Input is likely not prime.'

    }

    if ( $Remainders[$Remainder] ) {

        [PSCustomObject]@{
            'Digits'    = $Attempt
            'Repeating' = $Remainder
        }
        break

    }

    # Fun fact: using the bracket notation for hashtable keys is thousands of times faster than dot notation!
    $Remainders[$Remainder] = $true

    $Remainder *= 10
    while ( $Remainder -lt $InputPrime ) {

        $Remainder *= 10
        $Attempt ++

    }

}