# Written by Colby Bouma
# This script clones all of a given user's Gists.
# Based on: https://gist.github.com/selimslab/958e2255a105f9a3f4b421896bce715d
# https://gitlab.com/ColbyBouma/misc-scripts/-/blob/master/PowerShell/Get-GitHubGists.ps1

#Requires -Version 7

[CmdletBinding()]
param (
    [Parameter(Mandatory = $true)]
    [String]$User
)

#TODO: Make sure git is installed.

function Get-Gists {
    param (
        $GistPage
    )

    ForEach ( $Gist in $GistPage ) {

        # Grab the name of the first file.
        $Name = ($Gist.files.PSObject.Properties | Where-Object MemberType -eq NoteProperty).Name | Select-Object -First 1
        # Remove file extension(s).
        $Name = $Name -split '\.' | Select-Object -First 1

        #TODO: If the folder already exists, do a git pull.
        Write-Verbose -Message "Cloning $($Gist.id) to '$Name'"
        git clone $Gist.git_pull_url "$Name"

        # Skip the Description if it's empty.
        if ( $Gist.description ) {

            $Gist.description | Out-File -FilePath "$Name\description.txt"

        }

        #TODO: Download comments.

    }

}

$PageNumber = 1
Do {

    $GistPage = Invoke-RestMethod -Uri "https://api.github.com/users/$User/gists?page=$PageNumber"
    
    # Pages that don't exist return null.
    if ( $GistPage ) {

        Get-Gists -GistPage $GistPage
        $PageNumber ++

    }

} Until ( -not $GistPage )