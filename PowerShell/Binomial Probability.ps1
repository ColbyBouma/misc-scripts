
[CmdletBinding()]
param (
    [UInt32]$Trials,
    [Double]$ProbabilityOfSuccess
)

function Factorial {

    [CmdletBinding()]
    param (
        [UInt32]$InputNumber
    )

    [BigInt]$Result = 1

    while ( $InputNumber -gt 1 ) {

        $Result *= $InputNumber
        $InputNumber --

    }

    $Result

}



$ProbabilityOfFailure = 1 - $ProbabilityOfSuccess

for ( $Successes = 0; $Successes -le $Trials; $Successes ++ ) {

    # https://probabilityformula.org/binomial-probability-formula/
    # https://byjus.com/binomial-probability-formula/
    [Double]$Bernouli = (Factorial $Trials) / ((Factorial $Successes) * (Factorial ($Trials - $Successes)))
    $SuccessSection = [Math]::Pow($ProbabilityOfSuccess, $Successes)
    $FailureSection = [Math]::Pow($ProbabilityOfFailure, ($Trials - $Successes))
    $Result = $Bernouli * $SuccessSection * $FailureSection * 100

    [PSCustomObject]@{
        'Successes'         = $Successes
        'Chance Percentage' = $Result
        'One out of'        = [Math]::Round((100 / $Result), 2)
    }

}