# Written by Colby Bouma
# Tests the max recursion depth of PowerShell. Results will vary by PS version and system specs.
# https://gitlab.com/ColbyBouma/misc-scripts/-/blob/master/PowerShell/Recursion.ps1

function Recurse {
    
    param (
        [UInt64]$Num
    )

    $Num ++
    $Num
    Recurse $Num

}

Recurse 0