[[_TOC_]]

# WARNING

This is pre-release software and should not be executed on machines/drives you care about or which contain data that you wish to keep.
Downloading the software indicates that you accept any and all risks, including total data loss and hardware damage.



# SpinRite Alphas

You don't need access to Steve's GitLab instance to get the latest SpinRite 6.1 Alpha, but you will need a new or existing license for [SpinRite 6.0](https://www.grc.com/cs/prepurch.htm).
The latest Alpha is available directly from the GRC website here:  
https://www.grc.com/prerelease.htm

Enter your serial number or transaction code.
The transaction code is in the email you received when you bought your license.
The serial number shows up on several screens when you run SpinRite.
If you can't find either of those, send an email to the address in the 3rd question of the [FAQ](https://www.grc.com/sr/faq.htm).

Check later threads started by Steve Gibson for the most up-to-date release information.

You can also use this script to automate the download process:  
https://gitlab.com/ColbyBouma/misc-scripts/-/blob/master/PowerShell/Download%20SpinRite%20Pre-Release.ps1

The Alphas are currently DOS-only executables, so you'll need a bootable DOS drive in order to run them.
[GRC's InitDisk](https://www.grc.com/initdisk.htm) utility will create one using a USB stick attached to your Windows machine. You can then copy the SpinRite Alpha to that drive.

Make sure to run it with the `freedos` parameter from an admin command prompt or PowerShell window, like so:

```
initdisk.exe freedos
```


# GRC's GitLab

Major events are always posted to the newsgroup, so you only need an account in GitLab if you wish to post or follow issues.
The sign-in process is detailed in the following post:  
https://grc.com/groups/spinrite.dev:38266

Once past that gate, you will be asked to create an account.
After doing so, you might be interested in the "How can you test SpinRite?" article here:  
https://dev.grc.com/Steve/spinrite-v6.1/-/wikis/How-can-you-test-SpinRite%3F

Other Wiki articles are shown in the right-hand column of that page.

Please ensure that you are running the latest release and check to see if your issue has already been reported in the Open Issues before creating a new one.
As the list can be quite long, also try searching for keywords related to your issue such as "red drive" or "crash".  
https://dev.grc.com/Steve/spinrite-v6.1/-/issues

Turn on Notifications in the right-hand column of the following issue to be alerted of new releases.
Please do NOT post comments in it:  
https://dev.grc.com/Steve/spinrite-v6.1/-/issues/185

Welcome aboard!



This page is based on RobAllen's post: https://grc.com/groups/spinrite.dev:43722