; Written by Colby Bouma
; This outputs the current time in my desired format.
; https://gitlab.com/ColbyBouma/misc-scripts/-/blob/master/AutoHotkey/Timestamp.ahk

; CTRL + WIN + T
^#t::
{
	
	Sleep, 200
	
    ; https://www.autohotkey.com/docs/commands/FormatTime.htm
	FormatTime, TimeStamp, , yyyy-MM-dd HH:mm
    SendInput %TimeStamp%
	
}
Return