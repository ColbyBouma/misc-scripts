; Written by Colby Bouma
; Rewrites YouTube Shorts URLs to the regular viewer.
; https://gitlab.com/ColbyBouma/misc-scripts/-/blob/master/AutoHotkey/Shorts.ahk

; CTRL + WIN + S
^#s::
{
	
    Sleep, 200
	
    ; Highlight the URL.
    SendInput, {F6}
    Sleep, 200

    ; Copy the URL to the clipboard.
    ; https://www.autohotkey.com/docs/misc/Clipboard.htm
    SendInput, ^c
    Sleep, 300

    ; Edit the URL and place it back into the clipboard.
    ; https://www.autohotkey.com/docs/commands/StrReplace.htm
    Clipboard := StrReplace(Clipboard, "shorts/", "watch?v=")
    Sleep, 200

    ; Paste the new URL.
    SendInput, ^v
    Sleep, 200

    SendInput, {Enter}
	
}
Return